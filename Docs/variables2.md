There are a set of variables which are set for you already, and most of these cannot have values assigned to them.
These can contain useful information, which can be used by the script to know about the environment in which it is running.

(FR Un ensemble de variables a déjà été défini pour vous et aucune valeur ne peut leur être affectée.
Ils peuvent contenir des informations utiles que le script peut utiliser pour connaître l'environnement dans lequel il s'exécute.)

The first set of variables we will look at are $0 .. $9 and $#.
The variable $0 is the basename of the program as it was called.
$1 .. $9 are the first 9 additional parameters the script was called with.
The variable $@ is all parameters $1 .. whatever. The variable $*, is similar, but does not preserve any whitespace, and quoting, so "File with spaces" becomes "File" "with" "spaces". This is similar to the echo stuff we looked at in A First Script. As a general rule, use $@ and avoid $*.
$# is the number of parameters the script was called with. 

