On Unix-like operating systems, let is a builtin command of the Bash shell that evaluates arithmetic expressions.



Description

Using let is similar to enclosing an arithmetic expression in double parentheses, for instance:

(( expr ))

However, unlike (( ... )), which is a compound command, let is a builtin command. As such, just like a simple command, its individual arguments are subject to expansion by bash. For instance, let arguments will undergo globbing, pathname expansion, and word splitting unless you enclose the individual arguments in double quotes.
In the majority of situations, it's preferable to use double parentheses to evaluate arithmetic expressions. However, it's important to understand how let is different, and use it where appropriate.
Like all simple commands, let has its own environment. Variables used in expressions have scope local to the command. So, for instance, an argument to let will not be aware of other shell variables, unless they are exported.
The exception is whenever let evaluates an expression that explicitly sets a variable's value. In that case, that variable's value persists in the current shell. (If this sounds a little convoluted, that's because it is. See below for examples.)
Syntax

let arg [arg ...]



Description

let evaluates each argument, arg, as a math expression. Arguments are evaluated left to right.
All numbers are represented internally as fixed-width integers. (Bash does not support floating-point arithmetic.)
No overflow checking is performed, so operations on very large integers may produce an unexpected result, without causing an error.
Division by zero is detected, however, and causes an error.

Arithmetic operators
When evaluating an expression, let interprets the following mathematical operators, and performs the corresponding operation. In the table below, operators are listed row-by-row in order of decreasing precedence. Operators listed on the same row are of equal precedence.

var++, var--	Post-increment (++), Post-decrement (--). Interpret the value of integer variable var and then add or subtract one (1) to it.
++var, --var	Pre-increment (++), Pre-decrement (--). Add or subtract one (1) to the value of integer variable var, and then interpret the value.
-expr, +expr	Unary minus, Unary plus. Unary minus returns the value of the expression expr it precedes, as if it had been multiplied by negative one (-1). Unary plus returns the expression expr unchanged, as if it had been multiplied by one (1).
!, ~	Logical negation, Bitwise negation. Logical negation returns false if its operand is true, and true if the operand is false.
Bitwise negation flips the bits in the binary representation of the numeric operand.
**	Exponentiation.
*, /, %	Multiplication, Division, Remainder (modulo).
+, -	Addition, Subtraction.
<<, >>	Bitwise shift left, bitwise shift right.
<=,>=,<,>	Comparison: Less than or equal to, Greater than or equal to, Less than, Greater than.
==, !=	Equality, Inequality. Equality returns true if its operands are equal, false otherwise. Inequality returns true if its operands are not equal, false otherwise.
&	Bitwise AND. The corresponding binary digits of both operands are multiplied to produce a result; for any given digit, the resulting digit is 1 if and only if the corresponding digit in both operands is also 1.
^	Bitwise XOR (eXclusive OR). A binary digit of the result is 1 if and only if the corresponding digits of the operands differ. For instance, if the first binary digit of the first operand is 1, and the first digit of the second operand is 0, the first digit of the result is 1.
|	Bitwise OR. If either of the corresponding digits in the operands is 1, that digit in the result will also be 1.
&&	Logical AND. Returns true if both of the operands are true.
||	Logical OR. Returns true if either of the operands is true.
expr1 ? expr2 : expr3	Conditional (ternary) operator. If expr1 is true, return expr2. If expr1 is false, return expr3.
=, *=, /=, %=, +=, -=, <<=, >>=, &=, ^=, |=	Assignment. Assign the value of the expression that follows the operator, to the variable that precedes it. If an operator prefixes the equals sign, that operation is performed prior to assignment.
For instance, let "var += 5" is equivalent to let "var = var + 5". The assignment operation itself evaluates to the value assigned.



Exit status

If the rightmost argument provided to let evaluates to zero (arithmetically false), the exit status is 1 (FALSE exit status).
If the rightmost argument provided to let evaluates to non-zero (arithmetically true), the exit status is 0 (TRUE exit status).
