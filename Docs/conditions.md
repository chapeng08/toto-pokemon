A Basic Example

You can try these commands simply by opening a terminal window.

test 1 -eq 2 && echo "yes" || echo "no"

The above command can be broken down as follows:

    test - This means you are about to perform a comparison
    1 - The first element you are going to compare
    -eq - How are you comparing. In this case, you are testing whether one number equals another.
    2 - The element you are comparing the first element against 
    && - Run the following statement if the result is true
    echo "yes" - The command to run if the comparison returns true
    || - Run the following statement if the result is false
    echo "no" - The command to run if the comparison returns false

In essence, the command is comparing 1 to 2 and if they match, the echo "yes" statement is executed which displays "yes" and if they do not match, the echo "no" statement is executed which displays "no."


 Comparing Numbers

If you are comparing elements that parse as numbers you can use the following comparison operators:

    -eq - does value 1 equal value 2
    -ge - is value 1 greater or equal to value 2
    -gt - is value 1 greater than value 2
    -le - is value 1 less than or equal to value 2
    -lt - is value 1 less than value 2
    -ne - does value 1 not equal value 2


 Comparing Text

If you are comparing elements that parse as strings, you can use the following comparison operators:

    = - does string 1 match string 2
    != - is string 1 different to string 2
    -n - is the string length greater than 0
    -z - is the string length 0


 Comparing Files

If you are comparing files, you can use the following comparison operators:

    -ef - Do the files have the same device and inode numbers (are they the same file)
    -nt - Is the first file newer than the second file
    -ot - Is the first file older than the second file
    -b - The file exists and is block special
    -c - The file exists and is character special
    -d - The file exists and is a directory
    -e - The file exists
    -f - The file exists and is a regular file
    -g - The file exists and has the specified group number
    -G - The file exists and owner by the user's group
    -h - The file exists and is a symbolic link
    -k - The file exists and has its sticky bit set
    -L - The same as -h
    -O - The file exists you are the owner
    -p - The file exists and is a named pipe
    -r - The file exists and is readable
    -s - The file exists and has a size greater than zero
    -S - The file exists and is a socket
    -t - The file descriptor is opened on a terminal
    -u - The file exists and the set-user-id bit is set
    -w - The file exists and is writable
    -x - The file exists and is executable
