[20:11] <greycat> wt=(); for name in "$@"; do wt+=( "$(curl -s "https://pokeapi.co/api/v2/pokemon/$name/" | jq '.weight')" ); done; echo "${wt[@]}"
[20:12] <wuseman> qwebirc72167:  if ! [[ "$name" =~ ^[0-9]+$ ]]; then echo "$name not a valid pokemon"; fi
[20:12] <greycat> wuseman: no.
[20:12] <wuseman> but why using name= when you not using it in script?
[20:12] <vishal> wuseman: no need for regex, jq has a usable exit code
[20:12] <wuseman> ok
[20:13] <wuseman> I dont know JQ so I be quiet
[20:13] <greycat> the pokemon names aren't numeric, either
[20:13] <wuseman> but the output is
[20:13] <greycat> but you're not TESTING the output
[20:13] <vishal> lol
[20:13] <wuseman> Well then he should use a variable with all pokemons names then
[20:14] <wuseman> how else script will know
[20:14] <greycat> I already gave what should be a working script.
[20:14] <wuseman> yeah
[20:17] <wuseman> but did it solve your problem now qwebirc72167?
[20:18] <qwebirc72167> <wuseman> i think. i will try with ur suggestions. Nice!
[20:19] <wuseman> Perfect!
[20:19] <greycat> !$@
[20:19] <greybot> The difference between $@ and $*: "$@" (quoted) expands to each positional parameter as its own argument: "$1" "$2" ... while "$*" expands to the single argument "$1c$2c..." where c is the first character of IFS. Unquoted $* and $@ are undefined; DO NOT use. You almost always want "$@". The same goes for arrays: "${array[@]}"
[20:19] <greycat> that's the critical piece here
[20:19] <greycat> after that, is the array
[20:20] <greycat> jq is going to write each piece of output with a newline, but he wanted them space-separated on a single line, so we have to catch jq's output and reformat it.  the easiest way is to gather all the outputs into an array, and then print that array.
[20:20] <greycat> there are other ways it could be done, but this is the cleanest
[20:22] <knstn> Who wrote that part "http://mywiki.wooledge.org/Arguments" ? It's good.
[20:22] <greycat> https://mywiki.wooledge.org/Arguments?action=info
[20:29] <learning_2_learn> greycat: how to evaluate variable hold integer or string
[20:29] <greycat> !faq valid
[20:29] <greybot> https://mywiki.wooledge.org/BashFAQ/054 -- How can I tell whether a variable contains a valid number?
