#!/bin/bash


#Instructions : 
capture the output into a variable using: « weight=$( pokemon pikachu )  » and then test that it's right using [[ $weight = 1234 ]] || echo "result incorrect"
read « man bash », search for « command substitution


./toto.sh pikachu
weight=$(./toto.sh pikachu) 
[[ $weight = 60 ]] && echo "The result is correct" || echo "The result is incorrect"

./toto.sh ditto
weight=$(./toto.sh ditto) 
[[ $weight = 40 ]] && echo "The result is correct" || echo "The result is incorrect"

./toto.sh bulbasaur
weight=$(./toto.sh bulbasaur) 
[[ $weight = 69 ]] && echo "The result is correct" || echo "The result is incorrect"

./toto.sh ivysaur
weight=$(./toto.sh ivysaur) 
[[ $weight = 130 ]] && echo "The result is correct" || echo "The result is incorrect"

./toto.sh venusaur
weight=$(./toto.sh venusaur) 
[[ $weight = 1000 ]] && echo "The result is correct" || echo "The result is incorrect"
