#Other code same instructions (cf toto1.sh)

#!/bin/bash

test -n "$1" && name="$1"
#(cf Docs/conditiond.md) "test -n" Tester que la chaîne du premier argument est bien supérieur à 0. "&&"" Exécutez alors l'instruction suivante si le résultat est vrai. Le premier argument est une variable que l'on nommera Name 
curl -s "https://pokeapi.co/api/v2/pokemon/${name}/" | jq '.weight'