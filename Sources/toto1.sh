#Instructions:Créer un fichier .sh qui et lorsqu'on l'éxécute ./toto.sh (nom du pokémon) il nous renvoie comme résultat le poids du pokémon cité. On exécute donc le fichier avec un seul argument.


#!/bin/bash

name="$*" #(cf Docs/variables2.md) Une variable l'on va nommé "name" qui va correspondre à un argument quelconque.
if [ $1 = $* ] #Si le premier argument est un argument quelconque.
then
	curl -s "https://pokeapi.co/api/v2/pokemon/${name}/" | jq '.weight' #(cf Docs/curlandjq.md) Avec Curl on transfère les données depuis le serveur en utilisant le logiciel de prise en charge jq. Et on désactive complètement le compteur de progression avec -s (option silence). Enfin Utilisation de curl et de jq pour analyser les charges utiles des réponses. Ici uniquement le poids.
fi

#Remarques: $* mauvais choix. J'aurai dû utiliser à la place $@.  La variable $ * est similaire, mais ne conserve pas les espaces et les citations. Et de manière globale, ce programme est incomplet. Notamment si l'user l'exécute avec une variable erronnée.

