# Other code same instructions (cf toto1.sh). Addition in this code: execute toto with multiple arguments. The results on the same line.

weight=();
for name in "$@"; #(cf forloop.md) Remarque: $@ n'a aucun sens sans les " ".  
do weight+=( "$(curl -s "https://pokeapi.co/api/v2/pokemon/$name/" | jq '.weight')" ); #Exécuter en boucle la liste de l'ensemble des arguments.
done;
        echo "${weight[@]}" # (cf arrays.md) Afficher une liste de toutes les valeurs de weight. 
