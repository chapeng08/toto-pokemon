#Other code same instructions (cd toto1.sh). Specifity: code that include test. Only for one argument.

#!/bin/bash

name=''
weight=''

if test $# -gt 1;
#(comparing number) Si le nombre d'arguments avec lequel le script a été appelé est supérieur à 1
then
    echo "You have entered more than 1 argument. This script takes the name of a pokemon as the ONLY parameter."
    exit 1
fi

if test -n "$1";
#(comparing text) Si la chaîne du premier argument est supérieur à 0
then
    name="$1"
    weight=$( curl -s "https://pokeapi.co/api/v2/pokemon/${name}/" | jq '.weight' )

if test -n "$weight" -a "$weight" = "$weight";
then
        echo "$weight"
        exit 0
else
        echo "Result '$weight' is invalid or not an integer."
        exit 1
fi
else
        echo "You have failed to enter a pokemon id as the first (and only) parameter."
        exit 1
fi
