# Other code same instructions (cf toto1.sh). Run the program with multiple arguments. The result gives the average of all the weights.

#!/bin/bash

weight=()
for name in "$@";
do let weight+=( "$(curl -s "https://pokeapi.co/api/v2/pokemon/$name/" | jq '.weight')" ); #"let weight+=" (cf Docs/let.md) pour attribuer la valeur de l'expression qui suit l'opérateur à la variable qui le précède.
done;
        echo "$weight / ${#@}" | bc ; #(cf Docs/averrages.md ) Afficher la somme de toutes les variables puis le diviser par le nombre total d'arguments.
