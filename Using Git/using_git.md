Check ur informations:
git config --global --list


Initialize a local directory for Git version control:
git init


Go to the master branch to pull the latest changes from there:
git checkout master


Download the latest changes in the project:
This is for you to work on an up-to-date copy (it is important to do this every time you start working on a project), while you set up tracking branches. You pull from remote repositories to get all the changes made by users since the last time you cloned or pulled the project. Later, you can push your local commits to the remote repositories.
git pull origin master


Add and commit local changes:
git add FILE OR FOLDER
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"


Add all changes to commit
To add and commit all local changes in one command:
git add .
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"


Send changes to GitLab.com
To push all local commits to the remote repository:
git push origin master


Delete all changes in the Git repository:
To delete all local changes in the repository that have not been added to the staging area, and leave unstaged files/folders, type:
git checkout .


Delete all untracked changes in the Git repository:
git clean -f


Unstage all changes that have been added to the staging area
To undo the most recent add, but not committed, files/folders:
git reset .


To undo the most recent commit, type:
git reset HEAD~1
